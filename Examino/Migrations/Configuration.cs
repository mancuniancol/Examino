using System;
using System.Data.Entity.Migrations;
using System.Linq;
using Examino.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Examino.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //Ajouter de Roles
            context.Roles.AddOrUpdate(m => m.Name, new IdentityRole {Name = "Student"});
            context.Roles.AddOrUpdate(m => m.Name, new IdentityRole {Name = "Teacher"});
            context.Roles.AddOrUpdate(m => m.Name, new IdentityRole {Name = "Director"});
            context.Roles.AddOrUpdate(m => m.Name, new IdentityRole {Name = "Admin"});
            //Ajouter la commpte d'administration
            context.Users.AddOrUpdate(m => m.UserName, new ApplicationUser
            {
                UserName = "admin@isi-mtl.com",
                Email = "admin@isi-mtl.com",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                PasswordHash = new PasswordHasher().HashPassword("Abc123...")
            });
            context.SaveChanges();
            var user = context.Users.SingleOrDefault(d => d.UserName == "admin@isi-mtl.com");
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            userManager.AddToRole(user.Id, "Admin");
        }
    }
}